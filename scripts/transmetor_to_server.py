#!/usr/bin/env python

##################
# Imports
##################
import roslib
import rospy
import requests
import datetime
import json

from std_msgs.msg import Float32, String

roslib.load_manifest('bridge_to_web_server')

##################
# Constants
##################
ADR_SERVER = '35.166.111.184'
PORT_SERVER = '8080'
API_SENSOR = 'sensor'
API_ALERTS = 'alerts'
idRobot = 7
hum = -999
temp = -999
felt = -999

##################
# Functions
##################

# Main function to send the data to the server
def sendToServer(data, type):
  global hum
  global temp
  global felt
  # construct the date of today
  d = datetime.datetime.now();
  date_to_send = str(d.year).zfill(4)+'-'+str(d.month).zfill(2)+'-'+str(d.day).zfill(2)+'T'+str(d.hour).zfill(2)+':'+str(d.minute).zfill(2)+':'+str(d.second).zfill(2)   

  if type == 'hum':
    hum = data.data
  elif type == 'temp':
    temp = data.data
  elif type == 'felt':
    felt = data.data
  #ENDIF

  if (hum != -999 and temp != -999 and felt != -999):
    # Construct the url to request the server api
    url = 'http://'+ADR_SERVER+':'+PORT_SERVER+'/'+API_SENSOR
    
    # Construct a JSON object to send to the server
    data = {'date'      : date_to_send,
            'value_hum' : hum, 
            'value_temp': temp, 
            'value_felt': felt,
            'idRobot'   : idRobot}
    headers = {'Content-Type': 'application/json'}

    # launch the request
    r = requests.post(url, data=json.dumps(data), headers=headers)
    hum  = -999
    temp = -999
    felt = -999

    # return the response to the caller
    return json.dumps(r.json(), indent=4)    
  #ENDIF
#ENDDEF

# function to send alerts to the server
def sendAlerts(data):
  print 'implement todo'
#ENDDEF

# Start the listenic of the topics
def listener():
  # differents topic to subscrite for the datas
  rospy.Subscriber('humidity', Float32, sendToServer, 'hum')
  rospy.Subscriber('temperature', Float32, sendToServer, 'temp')
  rospy.Subscriber('felt_temp', Float32, sendToServer, 'felt')
  rospy.Subscriber('alerts', String, sendToServer)
  
  print "Ready to add sensors values or alerts. \n"
  # waiting something happened
  rospy.spin()
#ENDDEF

##################
# Start the node
##################
if __name__ == '__main__':
  rospy.init_node('transmetor_to_server')
  listener()
#ENDIF
