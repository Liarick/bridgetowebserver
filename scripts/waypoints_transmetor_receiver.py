#!/usr/bin/env python
import roslib
import rospy
import requests

from bridge_to_web_server.srv import *
from std_msgs.msg import Int32
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion

roslib.load_manifest('bridge_to_web_server')

##################
# Constants
##################
ADR_SERVER = '35.166.111.184'
PORT_SERVER = '8080'
API_WAYPOINTS_REQUEST = 'waypoints'
API_MAPS_REQUEST = 'maps'
idRobot = 7

##################
# Functions
##################


# function to add waypoints to the server
def handle_add_waypoints(req):
    success, idMap = send_map(req.idRobot, req.map.data, req.map.info)
    if success:
        if send_waypoints(idMap, req.pathToSend.poses, req.idRobot):
            response = PathToServerResponse()
            response.success = True
            response.message = 'Successfully send'
            return response
    response = PathToServerResponse()
    response.message = 'Error during server call'
    response.success = False

    return response
# ENDDEF


def send_waypoints(idMap, path, idRobot):
        # server adress construction for the waypoints
    adr_to_send = 'http://' + ADR_SERVER + ':' + PORT_SERVER + '/' + API_WAYPOINTS_REQUEST + '/add'

    # a loop to send each waypoints
    for waypoint in path:
        wp_pose = waypoint.pose.position
        wp_orientation = waypoint.pose.orientation
        payload = {'idMap': idMap, 'idRobot': idRobot, 'wp_x': wp_pose.x, 'wp_y': wp_pose.y, 'wp_z': wp_pose.z, 'wpo_x': wp_orientation.x, 'wpo_y': wp_orientation.y, 'wpo_z': wp_orientation.z, 'wpo_w': wp_orientation.w}

        # the request to the server
        r = requests.post(adr_to_send, params=payload)
    # ENDFOR
    return r.status_code == 200


def send_map(idRobot, map_data, map_metadata):
    # server adress construction for the maps
    adr_to_send = 'http://' + ADR_SERVER + ':' + PORT_SERVER + '/' + API_MAPS_REQUEST + '/add'

    payload = {'meta_data': {}, 'idRobot': idRobot}

    payload['resolution'] = map_metadata.resolution
    payload['width'] = map_metadata.width
    payload['height'] = map_metadata.height
    payload['position'] = '{0},{1},{2}'.format(map_metadata.origin.position.x, map_metadata.origin.position.y, map_metadata.origin.position.z)
    payload['orientation'] = '{0},{1},{2},{3}'.format(map_metadata.origin.orientation.x, map_metadata.origin.orientation.y, map_metadata.origin.orientation.z, map_metadata.origin.orientation.w)

    f = open('/tmp/map', 'w')
    map_str = ','.join(map(str, [map_data]))

    map_str = map_str.replace('(', '')
    map_str = map_str.replace(')', '')
    map_str = map_str.replace(' ', '')
    f.write(map_str)  # python will convert \n to os.linesep
    f.close()

    files = {'map': open('/tmp/map', 'rb')}

    # the request to the server
    r = requests.post(adr_to_send, params=payload, files=files)

    if r.status_code == 200:
        return True, r.json()['id']
    else:
        return False, 0


# function to get waypoints from the server
def handle_get_waypoints(req):
    # payload is a dictionnary for paramaters to send in the http request
    payload = {'idRobot': req.idRobot}
    response = PathFromServerResponse()
    # server adress construction
    adr_to_send = 'http://' + ADR_SERVER + ':' + PORT_SERVER + '/' + API_WAYPOINTS_REQUEST + '/get'

    # send the request
    r = requests.get(adr_to_send, params=payload)

    if r.status_code != 200:
        response.success = False
        return response

    response.success = True
    path = Path()
    path.header.frame_id = 'map'

    list_of_wp = sorted(r.json(), key=lambda x: x['idwaypoints'])

    # creating a Path to return to the robot
    for wp in list_of_wp:
        pose = PoseStamped()
        pose.header.frame_id = 'map'
        pose.pose = Pose(Point(wp['wp_x'], wp['wp_y'], wp['wp_z']), Quaternion(wp['wpo_x'], wp['wpo_y'], wp['wpo_z'], wp['wpo_w']))
        path.poses.append(pose)
    # ENDFOR

    list_of_wp.sort(key=lambda x: x['idwaypoints'], reverse=True)

    # adding the return path to the Path for the robot
    for wp in list_of_wp:
        pose = PoseStamped()
        pose.header.frame_id = 'map'
        pose.pose = Pose(Point(wp['wp_x'], wp['wp_y'], wp['wp_z']), Quaternion(wp['wpo_x'], wp['wpo_y'], wp['wpo_z'], wp['wpo_w']))
        path.poses.append(pose)
    # ENDFOR

    response.path = path

    return response
# ENDDEF


# Start the listenic of the services calls
def listener():
        rospy.Service('add_waypoints', PathToServer, handle_add_waypoints)
        rospy.Service('get_waypoints', PathFromServer, handle_get_waypoints)

        rospy.loginfo("[BRIDGGE] Ready to add or get waypoints. \n")

        rospy.spin()
# ENDDEF


if __name__ == "__main__":
        rospy.init_node('waypoints_on_server')
        listener()
# ENDIF
